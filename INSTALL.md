FHWS_REST
=========

Maven:
------

#####1. Install Maven
 -  Add to settings.xml ("home"/.m2/settings.xml):

```xml
<servers>
  <server>
    <id>dev-tomcat</id>
    <username>username</username>
    <password>userpassword</password>
  </server>
</servers>
```
#####2. Install Maven plugin

 - Repository: choose normal update site (e.g. `http://download.eclipse.org/releases/juno`) 
 - Plugin: m2e
 
#####3. Config Maven plugin

 - window -> Preferences
 - Maven -> Installation
 - Add -> choose Maven installation directory
 - User settings -> choose settings.xml

 

Tomcat:
-------

#####1. Install Tomcat: http://tomcat.apache.org/download-70.cgi
#####2. Add to tomcat-user.xml:

```xml
<tomcat-users>
  <role rolename="manager-gui"/>
  <role rolename="manager-script"/>
  <role rolename="admin-gui"/>
  <user username="username" password="userpassword" roles="manager-gui,manager-script,admin-gui"/>
</tomcat-users>
```

GIT:
---

#####1. Install git from http://git-scm.com/

 - (Use git Bash only (recommended))
 - (Checkout Windows-style, commit Unix-style line endings)

#####2. Install git plugin:

 -  Repository: `http://download.eclipse.org/egit/updates/`
 -  Plugin: Eclipse Git Team Provider



Import into Eclipse:
--------------------

1. File -> Import -> Projects from Git
2. Clone Uri
3. Set Uri to `https://github.com/LukasLeppich/FHWS_REST.git`
4. Select master branche
5. Choose local directory and set a remote name
6. Import existing project
7. Select fhws project
8. Finish
9. Right click on project -> Maven -> Update Project


Build Project:
-------------

1. Right click on project and select Run as Maven Build ...
2. Set Goals to `clean install tomcat7:redeploy` this goals build the project and deploys it to the tomcat server. 
