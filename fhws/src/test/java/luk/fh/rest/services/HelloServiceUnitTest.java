package luk.fh.rest.services;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class HelloServiceUnitTest {
	private HelloService serviceObject;
	@Before
	public void init(){
		serviceObject = new HelloService();
	}
	
	@Test
	public void testHelloWorld(){
		assertEquals("Hello World!", serviceObject.helloWorld());
	}
	
	@Test
	public void testHelloEcho(){
		String echo = "Test String";
		String exprect = "Hello " + echo + "!";
		assertEquals(exprect, serviceObject.helloEcho(echo));
	}
}
