package luk.fh.rest.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;


@Path("/hello")
public class HelloService {

	@GET
	@Path("/world")
	public String helloWorld(){
		return "Hello World!";
	}
	@GET
	@Path("/{name}")
	public String helloEcho(@PathParam("name") String name){
		return "Hello " + name + "!";
	}
}
